/* jshint node: true */
"use strict" ;

import gulp from 'gulp' ;

import { compile }   from './build/compile.js' ;
import { doc }       from './build/doc.js' ;
import { install }   from './build/install.js' ;
import { update }    from './build/update.js' ;
import { minify }    from './build/minify.js' ;
import { unittests } from './build/unittests.js' ;

gulp.task( 'default'  , gulp.series( update , unittests , compile , minify ) ) ;
gulp.task( 'build'    , gulp.series( compile , minify ) ) ;
gulp.task( 'make'     , gulp.series( compile ) ) ;
gulp.task( 'compress' , gulp.series( minify ) ) ;
gulp.task( 'doc'      , gulp.series( doc ) ) ;
gulp.task( 'ut'       , gulp.series( unittests ) ) ;

gulp.task( 'install' , gulp.series( install ) ) ;
gulp.task( 'update'  , gulp.series( update ) ) ;