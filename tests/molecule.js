'use strict' ;

// CHAI Doc : http://chaijs.com/api/assert/

import chai from 'chai' ;

import { molecule } from '../src/molecule.js' ;

const assert = chai.assert ;

describe( 'molecule' , () =>
{
    it('molecule !== null', () =>
    {
        assert.isNotNull( molecule );
    });
});