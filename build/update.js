"use strict" ;

// --------- Imports

import config from '../config.json' ;

import fs     from 'fs-extra' ;
import git    from 'gulp-git' ;
import gulp   from 'gulp' ;
import util   from 'gulp-util' ;

// --------- Initialize

var colors = util.colors ;
var log    = util.log ;

var pullRepository = ( repo , done ) =>
{
    fs.exists( config.libs + '/' + repo.path , ( exists ) =>
    {
        if( exists )
        {
            git.checkout( '.' , { args :'--' , cwd  : config.libs + '/' + repo.path },
            function (error)
            {
                if (error)
                {
                    log( colors.red( error ) ) ;
                    done() ;
                    return ;
                }
                git.pull( 'origin' , repo.branche , { cwd : config.libs + '/' + repo.path } ,
                function ( error )
                {
                    if (error)
                    {
                        log( colors.red( error ) ) ;
                        done() ;
                        return ;
                    }
                    done() ;
                });
            });
        }
        else
        {
            log( colors.red( 'pull failed, the directory ' + config.libs + '/' + repo.path + ' don\'t exist' ) ) ;
        }
    });
}

var pullPixi = ( done ) =>
{
    pullRepository( config.repos.pixijs , done ) ;
}

var pullVegas = ( done ) =>
{
    pullRepository( config.repos.vegasjs , done ) ;
}

export var update = gulp.series( pullVegas , pullPixi ) ;