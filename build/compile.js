"use strict" ;

import babel        from 'rollup-plugin-babel' ;
import babelrc      from 'babelrc-rollup';
import cleanup      from 'rollup-plugin-cleanup';
import gulp         from 'gulp' ;
import header       from 'gulp-header' ;
import includePaths from 'rollup-plugin-includepaths';
import replace      from 'rollup-plugin-replace';
import rollup       from 'rollup-stream' ;
import source       from 'vinyl-source-stream' ;
import util         from 'gulp-util' ;

import config  from '../config.json' ;
import setting from '../package.json' ;
import vegas   from '../libs/vegas-js/package.json' ;

//var cache   = cache ;
var colors  = util.colors ;
var log     = util.log ;

var metas =
{
    NAME        : setting.name ,
    DESCRIPTION : setting.description ,
    HOMEPAGE    : setting.homepage ,
    LICENSE     : setting.license ,
    VERSION     : setting.version
}

export var compile = ( done ) =>
{
    return rollup
    ({
        moduleName : config.name ,
        entry      : config.entry ,
        format     : 'umd' ,
        sourceMap  : false ,
        useStrict  : true ,
        //cache      : cache ,
        globals    :
        {
            //
        },
        plugins    :
        [
            replace
            ({
                delimiters : [ '<@' , '@>' ] ,
                values     : metas
            }),
            includePaths
            ({
                include    : { },
                paths      : [ './src/' , './libs/vegas-js/src/' ] ,
                external   : [],
                extensions : [ '.js' , '.json' ]
            }) ,
            babel
            (
                babelrc( { addExternalHelpersPlugin : true } )
            ),
            cleanup()
        ]
    })
    .on('error', ( error ) =>
    {
        log( colors.magenta( `${error.stack}` ) );
        done() ;
    })
    // .on('bundle', function( bundle )
    // {
    //     //cache = config.cache ? bundle : null ;
    // })
    .pipe( source( config.name + '.js' ) )
    .pipe( header( config.header , { vegas : vegas.version , molecule : setting.version } ) )
    .pipe( gulp.dest( config.output ) );
}