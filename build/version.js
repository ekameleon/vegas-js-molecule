"use strict" ;

import molecule from '../package.json' ;
import vegas    from '../libs/vegas-js/package.json' ;

export var version =
{
    molecule : molecule.version ,
    vegas    : vegas.version
}