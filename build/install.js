"use strict" ;

// --------- Imports

import config from '../config.json' ;

import fs     from 'fs-extra' ;
import git    from 'gulp-git' ;
import gulp   from 'gulp' ;
import util   from 'gulp-util' ;

// --------- Initialize

var colors = util.colors ;
var log    = util.log ;

var clear = ( done ) =>
{
    fs.emptyDir( config.libs , function( error )
    {
        if (error)
        {
            log( colors.red( error ) ) ;
            done() ;
            return ;
        }
        done() ;
    }) ;
}

var clone = ( repo , done ) =>
{
    git.clone( repo.git , { cwd : config.libs , quiet: false } , function( error )
    {
        if (error)
        {
            log( colors.red( error ) ) ;
            done() ;
            return ;
        }
        done() ;
    }) ;
}

var vegasjs = ( done ) =>
{
    clone( config.repos.vegasjs , done ) ;
}

var pixijs = ( done ) =>
{
    clone( config.repos.pixijs , done ) ;
}

export var install = gulp.series( clear , vegasjs , pixijs ) ;