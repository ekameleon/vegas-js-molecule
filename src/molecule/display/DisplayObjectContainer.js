"use strict" ;

/**
 * The abstract class of all display groups.
 * @summary The abstract class of all display groups.
 * @name DisplayObject
 * @class
 * @memberof molecule.display
 * @extends PIXI.DisplayObject
 */
export var DisplayObjectContainer = PIXI.Container ;