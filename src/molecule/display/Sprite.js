"use strict" ;

/**
 * The animated sprite.
 * @summary The animated sprite.
 * @name Sprite
 * @class
 * @memberof molecule.display
 * @extends PIXI.extras.AnimatedSprite
 */
export var Sprite = PIXI.extras.AnimatedSprite ;