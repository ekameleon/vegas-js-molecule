"use strict" ;

import { DisplayObjectContainer } from './display/DisplayObjectContainer.js' ;
import { Sprite }                 from './display/Sprite.js' ;

/**
 * The {@link molecule.display} library is the root package for the <strong>MOLECULE JS</strong> library.
 * <p><b>Dependencies :</b> The {@link system} framework reuse the module and building blocks of the {@link core} library.</p>
 * @summary The {@link molecule} library is the root package for the <strong>MOLECULE JS</strong> library.
 * @license {@link https://www.mozilla.org/en-US/MPL/1.1/|MPL 1.1} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace molecule.display
 * @memberof molecule
 * @version 1.0.0
 * @since 1.0.0
 */
export var display = Object.assign
({
    DisplayObjectContainer : DisplayObjectContainer ,
    Sprite                 : Sprite
}) ;