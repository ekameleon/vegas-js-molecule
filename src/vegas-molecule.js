"use strict" ;

// import * as PIXI from '../libs/pixi.js/src/index.js';
//
// export { PIXI } ;

/**
 * The VEGAS.js framework.
 * @license {@link https://www.mozilla.org/en-US/MPL/1.1/|MPL 1.1} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 */
export
{
    global, trace, metas, version,
    sayHello, skipHello,
    core, system, graphics
}
from './vegas.js' ;

/**
 * The vegas.molecule.js framework.
 * @license {@link https://www.mozilla.org/en-US/MPL/1.1/|MPL 1.1} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 */
export { molecule } from './molecule.js' ;
