# MOLECULE JS#

**MOLECULE JS** is an *Opensource* Library based on **[VEGAS JS (V1)](https://bitbucket.org/ekameleon/vegas-js)** and **[PixiJS (V4)](http://www.pixijs.com/)** for develop crossplatform **Rich Internet Applications** and **Games**.

This project contains a set of libraries writing in **Javascript** and based on the [ES6](http://es6-features.org/) standard.

### About

 * Author : Marc ALCARAZ (aka eKameleon)
 * Mail : ekameleon[at]gmail.com
 * Link : [http://www.ooopener.com](http://www.ooopener.com)

### License

Under tree opensource licenses :

 * [License MPL 1.1](http://www.mozilla.org/MPL/MPL-1.1.html)
 * [License GPL 2](http://www.gnu.org/licenses/gpl-2.0.html)
 * [License LGPL 2.1](http://www.gnu.org/licenses/lgpl-2.1.html)

## Resources

#### ⌜ Download

Download on **Bitbucket** the latest code, report an issue, ask a question or contribute :

 * [http://bitbucket.org/ekameleon/vegas-js-molecule](http://bitbucket.org/ekameleon/vegas-js-molecule)

#### ⌜ Documentation

Get started with the the **Vegas JS** API :

 * [http://vegasjs.ooopener.com/](http://vegasjs.ooopener.com/)

#### ⌜ Slack Community

![slack-logo-vector-download.jpg](https://bitbucket.org/repo/AEbB9b/images/3509366499-slack-logo-vector-download.jpg)

Send us your email to join the **VEGAS OPENSOURCE** community on Slack !


## Builds

**VEGAS JS** use [Gulp](http://gulpjs.com/) and [Yarn](https://yarnpkg.com/) with a serie of powerful packages (Babel, Mocha, etc.) to build.

#### ⌜ Simple Build

1 - The first time, clone the **VEGAS JS** repository

2 - Initialize the project
```
yarn
```

**Note:** See the <code>gulpfile.babel.js</code> file at the top of the project folder.


## Builds

**VEGAS JS** use [Gulp](http://gulpjs.com/) and [Yarn](https://yarnpkg.com/) with a serie of powerful packages (Babel, Mocha, etc.) to build.


#### ⌜ Simple Build

1 - The first time, clone the **VEGAS JS** repository

2 - Initialize the project
```
yarn
```

**Note:** See the <code>package.json</code>, <code>config.json</code>, <code>gulpfile.babel.js</code> files at the top folder of the project.

3 - Install the VEGAS JS libraries (dependency)
```
gulp install
```

**Note:** To update the VEGAS JS repository use the command :
```
gulp update
```

4 - Build the binaries (**bin/molecule.js** and **bin/molecule.min.js**)
```
gulp
```
or 
```
gulp build
```
